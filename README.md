Notre pipeline CI/CD contient un seul job : test.
Avant de lancer le job, gitlab CI exute premièrement la commande "pip install -r requirements.txt" afin d'installer les packages nécessaires au projet django.
Après quoi le job de test est exécuté en appelant la commande "python manage.py test" avec "./db.sqlite3" comme chemin/url vers la base de données.

PS: Les jobs r2devops ont tous échoués d'où leur suppression dans cette dernier version.
